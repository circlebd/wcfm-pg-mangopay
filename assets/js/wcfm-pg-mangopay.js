(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	let app = {
		init : function() {
			this.setup();
			this.bindEvents();
			this.showHideRelevantFields();
			this.showHideMpStatusFileds();
		},

		setup : function() {
			this.document 			= $( document );
			this.bank_type  		= '.mangopay-type';
			this.dependent_class	= '.bank-type';
			this.user_mp_status 	= '#mangopay_user_mp_status';
			this.user_business_type = '#mangopay_user_business_type';
			this.mangopay_update_bi = '#update_business_information';
		},

		bindEvents : function() {
			this.document.on( 'change', this.bank_type, this.showHideRelevantFields.bind( this ) );
			this.document.on( 'change', this.user_mp_status, this.showHideMpStatusFileds.bind( this ) );
			this.document.on( 'click', this.mangopay_update_bi, this.updateBusinessInformation.bind(this));
		},

		showHideRelevantFields : function( event ) {
			let type;

			if( undefined == event ) {
				if( ! $( this.bank_type ).length ) return;
				type = $( this.bank_type ).val();
			} else {
				type = $( event.currentTarget ).val();
			}

			let dependent_field = {
				show : this.dependent_class + '.bank-type-' + type.toLowerCase(),
				hide : this.dependent_class + ':not(.bank-type-' + type.toLowerCase() + ')',
			};

			$( dependent_field.show ).each( function( key, value ) {
				$( value ).show()
				.prev( 'label' ).show()
				.prev( 'p' ).show(); 
			} );

			$( dependent_field.hide ).each( function( key, value ) {
				$( value ).hide()
				.prev( 'label' ).hide()
				.prev( 'p' ).hide();
			} );
		},

		showHideMpStatusFileds : function( event ) {
			let user_type;

			if( undefined == event ) {
				if( ! $( this.user_mp_status ).length ) return;
				user_type = $( this.user_mp_status ).val();
			} else {
				user_type = $( event.currentTarget ).val();
			}

			if( 'individual' === user_type ) {
				$( this.user_business_type ).hide()
				.prev( 'label' ).hide()
				.prev( 'p' ).hide();
			} else {
				$( this.user_business_type ).show()
				.prev( 'label' ).show()
				.prev( 'p' ).show();
			}
		},

		updateBusinessInformation : function( event ){
			event.preventDefault();
			let $loader = $('#ajax_loader');
			let $updated = $('#bi_updated');
			$loader.show();

			let compagny_number = $('#mangopay_compagny_number').val();
			let vendor_id = $('#mangopay_vendor_id').val();
			let headquarters_addressline1 = $('#mangopay_hq_address').val();
			let headquarters_addressline2 = $('#mangopay_hq_address2').val();
			let headquarters_city = $('#mangopay_hq_city').val();
			let headquarters_region = $('#mangopay_hq_region').val();
			let headquarters_postalcode = $('#mangopay_hq_postalcode').val();
			let headquarters_country = $('#mangopay_hq_country').val();

			if(
				compagny_number == '' ||
				headquarters_addressline1 == '' ||
				headquarters_addressline2 == '' ||
				headquarters_city == '' ||
				headquarters_region == '' ||
				headquarters_postalcode == '' ||
				headquarters_country == ''
			)
			{
				$loader.hide();
				$updated.html('Please insert all required fields!').css({'color': 'red'}).show();
				return;
			}else{
				$updated.html('');
			}

			if( compagny_number != '' ){
				$.ajax({
					type : "POST",
					dataType : "json",
					url : woodmart_settings.ajaxurl,
					data : {
						action: "update_mp_business_information",
						vendor_id: vendor_id,
						compagny_number : compagny_number,
						headquarters_addressline1 : headquarters_addressline1,
						headquarters_addressline2 : headquarters_addressline2,
						headquarters_city : headquarters_city,
						headquarters_region : headquarters_region,
						headquarters_postalcode : headquarters_postalcode,
						headquarters_country : headquarters_country,
					},
					success: function(response) {
						if(response.success) {
							$updated.html('Successfully updated!').css({'color': 'green'}).show();
							setTimeout(function(){ $updated.fadeOut(2000) }, 5000);
						}else{
							$updated.html(response.msg).css({'color': 'red'}).show();
							setTimeout(function(){ $updated.fadeOut(2000) }, 5000);
						}
					},
					complete: function(){	
						$loader.hide();
					}
				})
			} 
		}
	};

	$( app.init.bind( app ) );

})( jQuery );
